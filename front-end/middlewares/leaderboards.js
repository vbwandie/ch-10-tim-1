import { FETCH_SUCCESS } from '../store/actions/leaderboards';

// export function fetchLeaderboard(id) {
//   return async function fetchLeaderboardsThunk(dispatch, getState) {
//     const response = await fetch("http://localhost:4000/api/v1/games/detail?game_id="+id);
//     const result = await response.json();

//     console.log('data already fetched')

//     dispatch({
//       type: FETCH_SUCCESS,
//       payload: result.data
//     });
//   };
// }

export function fetchLeaderboards(id) {
  return async function fetchLeaderboardsThunk(dispatch, getState) {
    const response = await fetch("http://localhost:4000/api/v1/games/detail");
    const result = await response.json();

    console.log('data already fetched')

    dispatch({
      type: FETCH_SUCCESS,
      payload: result.data
    });
  };
}