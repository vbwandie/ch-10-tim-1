import { FETCH_SUCCESS } from '../store/actions/game-details';

export function fetchGameDetails(id) {
  return async function fetchGameDetailsThunk(dispatch, getState) {
    const response = await fetch("http://localhost:4000/api/v1/games/"+id);
    const result = await response.json();

    console.log('data already fetched')

    dispatch({
      type: FETCH_SUCCESS,
      payload: result.data
    });
  };
}
