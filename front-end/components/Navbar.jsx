import React, { useEffect, useState } from 'react';
// import { Nav, NavItem, NavLink } from "reactstrap";
import styles from '../styles/Navbar.module.css';
import Link from 'next/link';
import { Button } from 'reactstrap';
import { useSelector, useDispatch } from 'react-redux';

function Navbar() {
  const [click, setCLick] = useState(false);
  const [button, setButton] = useState(true);
  const handleClick = () => setCLick(!click);
  const closeMobileMenu = () => setCLick(false);
  const userState = useSelector((state) => state.users);

  const showButton = () => {
    if (window.innerWidth <= 960) {
      setButton(false);
    } else {
      setButton(true);
    }
  };

  useEffect(() => {
    showButton();
  }, []);

  return (
    <>
      <nav className={styles.navbar}>
        <div className={styles['navbar-container']}>
          <div className={styles['navbar-logo']} onClick={closeMobileMenu}>
            TOP GAMES
            <i className='fa-sharp fa-solid fa-dice-d6'></i>
          </div>
          <div className={styles['menu-icon']} onClick={handleClick}>
            <i className='fas fa-bars' />
          </div>

          <ul className={styles['nav-menu']}>
            <li className={styles['nav-item']}>
              <Link href='/' className={styles['nav-links']}>
                <a className={styles['game-link']}>Home</a>
              </Link>
            </li>
            <li className={styles['nav-item']}>
              <Link href='/games/list' className={styles['nav-links']}>
                <a className={styles['game-link']}>Game List</a>
              </Link>
            </li>
            <li className={styles['nav-item']}>
              <Link href='/login' className={styles['nav-links']}>
                <a className={styles['game-link']}>Login</a>
              </Link>
            </li>
            <li>
              <Link href='/register' className={styles['nav-links-mobile']}>
                <a className={styles['game-link']}>Register</a>
              </Link>
            </li>
            <li>
              <Link href='/profile' className={styles['nav-links-mobile']}>
                <a className={styles['game-link']}>Profile</a>
              </Link>
            </li>
          </ul>
        </div>
      </nav>
    </>
  );
}

export default Navbar;
