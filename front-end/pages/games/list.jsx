import { Component } from "react";
import Link from "next/link";
import { Card, CardBody, CardSubtitle, CardTitle, CardImg, Container } from "reactstrap";
import { useEffect } from "react"
import { useSelector, useDispatch } from "react-redux";

import styles from "../../styles/gamelist.module.css";
import { fetchGameList } from '../../middlewares/game-list'
import { fetchLeaderboards } from '../../middlewares/leaderboards'
import Navbar from "../../components/Navbar";

export default function GameList() {
  const gameListState = useSelector((state) => state.gameList);
  const leaderboardsState = useSelector((state) => state.leaderboards);
  const userState = useSelector((state) => state.users);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchGameList())
  }, []);

  useEffect(() => {
    dispatch(fetchLeaderboards())
  }, []);

  // To check if user already played the game
  const alreadyPlayed = (id) => {
    const played = false;
    for (const i=1; i < leaderboardsState.leaderboards.length; i++){
      if (leaderboardsState.leaderboards[i].game_id == id && userState.isLoggedIn == true) {
        played = true;
        break
      } 
      else {
        played = false
      }
    }
    return played
  }

  // Create Element Game List
  const createElementGameList = () => {
    let elements = [];
    for (const i in gameListState.gameList) {
      elements.push(
        <Card style={{ width: "18rem" }} className="m-3" key={i}>
          <CardImg top width="100%" src={gameListState.gameList[i].thumbnail_url} />
          <CardBody>
            <CardTitle tag="h5">
              <Link
                href={userState.isLoggedIn ? "/games/" + gameListState.gameList[i].id : "/login"}
                className={styles.removeHighlight}>
                {gameListState.gameList[i].name}
              </Link>
            </CardTitle>
            <CardSubtitle>{alreadyPlayed(gameListState.gameList[i].id) ? 'You already played this game' : '' }</CardSubtitle>
          </CardBody>
        </Card>
      );
    }
    return elements;
  }

  return (
    <>
        <Navbar />
      <Container className={styles.container + " d-block"}>
        <p className="d-flex justify-content-center h1 text-light">
          List of Available Games
        </p>
        <Container className="d-flex justify-content-center mt-5">
          {createElementGameList()}
        </Container>
      </Container>
    </>
    )
}

